#!/usr/bin/env bash

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

# Source: https://gitlab.mpcdf.mpg.de/tabriz/mgc
# Copyright 2022-2023 M. Farzalipour Tabriz, Max Planck Computing and Data Facility (MPCDF)

# Fails if there is any broken symlink is found.

if [[ "$1" == "--verbose" ]]; then echo "Testing symlinks..."; fi;

script_path=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
# shellcheck source-path=checks
source "${MGC_CHECKS_PATH:=$script_path}/.tools.sh"

link_err=0
dead_links=$(find . -xtype l)

if [[ -n "$dead_links" ]]; then
    printf "%bERROR: broken symlinks found!\n" "${color_red}"
    # shellcheck disable=SC2068
    for link in $dead_links; do
        ls -lhA "$link"
    done
    printf "%b" "${color_reset}"
    link_err=1
fi

if [[ "$link_err" == "0" ]]; then
    printf "%bPASS: no broken symlink was found.%b\n" "${color_green}" "${color_reset}"
else    
    export check_err=1
fi
