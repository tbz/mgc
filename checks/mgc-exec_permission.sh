#!/usr/bin/env bash

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

# Source: https://gitlab.mpcdf.mpg.de/tabriz/mgc
# Copyright 2022-2023 M. Farzalipour Tabriz, Max Planck Computing and Data Facility (MPCDF)

# Parse all files, if a file has shebang, confirm executable permissions on that file.
# Test ignores ./.git directory.


if [[ "$1" == "--verbose" ]]; then echo "Testing executable files permission..."; fi;

script_path=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
# shellcheck source-path=checks
source "${MGC_CHECKS_PATH:=$script_path}/.tools.sh"

exec_permission_err=0

files_list=$(find . -not -path './.git/*' -type f)

IFS=$'\n'
# shellcheck disable=SC2068
for file in $files_list; do
    if IFS= read -r line < "$file"; then
        if [[ "$1" == "--verbose" ]]; then echo "Checking: $file"; fi;
        case $line in
            "#!"*)
                if [[ "$1" == "--verbose" ]]; then echo "    has shebang!"; fi;
                if ! [[ -x "$file" ]]; then
                        printf "%bERROR: executable file with no permission was found: %s\n" "${color_red}" "$file"
                        ls -lhA "$file"
                        printf "%b" "${color_reset}"
                        exec_permission_err=1
                else
                    if [[ "$1" == "--verbose" ]]; then stat -c "    has exec permission: %A" "$file"; fi;
                fi
            ;;
            *)
                if [[ "$1" == "--verbose" ]]; then echo "   has no shebang."; fi;
        esac
    fi
done    


if [[ "$exec_permission_err" == "0" ]]; then
    printf "%bPASS: no permission issue was found!%b\n" "${color_green}" "${color_reset}"
else
    export check_err=1
fi