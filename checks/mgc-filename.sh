#!/usr/bin/env bash

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

# Source: https://gitlab.mpcdf.mpg.de/tabriz/mgc
# Copyright 2022-2023 M. Farzalipour Tabriz, Max Planck Computing and Data Facility (MPCDF)

# Fails if any of files have unsafe characters in their name.
# unsafe: space

# TODO: check for ascii controll characters

if [[ "$1" == "--verbose" ]]; then echo "Testing file names..."; fi;

script_path=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
# shellcheck source-path=checks
source "${MGC_CHECKS_PATH:=$script_path}/.tools.sh"

name_err=0
space_in_name=$(find . | grep " ")

if [[ -n "$space_in_name" ]]; then
    printf "%bERROR: space in file names!\n" "${color_red}"
    # shellcheck disable=SC2068
    for file in $space_in_name; do
        ls -lhA "$file"
    done
    printf "%b" "${color_reset}"
    name_err=1
fi

if [[ "$name_err" == "0" ]]; then
    printf "%bPASS: no file with unsafe name was found.%b\n" "${color_green}" "${color_reset}"
else
    export check_err=1
fi
