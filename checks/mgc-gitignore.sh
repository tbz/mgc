#!/usr/bin/env bash

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

# Source: https://gitlab.mpcdf.mpg.de/tabriz/mgc
# Copyright 2022-2023 M. Farzalipour Tabriz, Max Planck Computing and Data Facility (MPCDF)

# This script checks all of the files in current git repository.
# If any of these files are filtered by .gitignore it will return an error.

if [[ "$1" == "--verbose" ]]; then echo "Testing gitignore lists..."; fi;

script_path=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
# shellcheck source-path=checks
source "${MGC_CHECKS_PATH:=$script_path}/.tools.sh"

requires_git

git_ignore_err=0
git_root_path=$(git rev-parse --show-toplevel 2>/dev/null)

if [[ -z "$git_root_path" ]]; then
    echo "ERROR: Could not find the top-level path of git repository!"
    exit 1;
fi

pushd "$git_root_path" > /dev/null || exit 1
git config --local advice.addIgnoredFile false

files_list=$(find . -not -path './.git/*' -type f)
IFS=$'\n'
# shellcheck disable=SC2068
for file in $files_list; do
    if [[ "$1" == "--verbose" ]]; then echo "Checking: $file"; fi;
    ignored=$(git check-ignore --no-index "$file")
    if [[ -n "$ignored" ]]; then
        printf "%bERROR: %s is ignored by git!\n" "${color_red}" "$file"
        git check-ignore --no-index -v "$file"
        printf "%b\n" "${color_reset}"
        git_ignore_err=1
    fi
done

popd > /dev/null || exit

if [[ "$git_ignore_err" == "0" ]]; then
    printf "%bPASS: no file in current git repository is ignored by your .gitignore files.%b\n" "${color_green}" "${color_reset}"
else
    export check_err=1
fi
