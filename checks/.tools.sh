# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

# Source: https://gitlab.mpcdf.mpg.de/tabriz/mgc
# Copyright 2022-2023 M. Farzalipour Tabriz, Max Planck Computing and Data Facility (MPCDF)

color_red="\033[31;49;1m"
color_green="\033[32;49;1m"
color_reset="\033[0m"


requires_git () {
    if ! [[ -x "$(command -v git)" ]]; then
        echo "ERROR: git tool not found!"
        exit 1;
    fi
}