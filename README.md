# Meisam's git checks

A set of bash scripts for checking git repositories.

## checks

- `checks/mgc-gitignore.sh`: file in the repository is ignored by `.gitignore`
- `checks/mgc-symlink.sh`: broken symlinks
- `checks/mgc-filename.sh`: space in file names
- `checks/mgc-exec_permission.sh`: file with shebang but no executable permission

## usage

> **Warning**
> These tests are meant to be run inside a container in CI environment. Running it locally, or in a privileged environment is not tested and therefore not recommended! 

Run all checks on the current path/git repository

```
mgc
```

Log each test's progress

```
mgc --verbose
```

print version

```
mgc --version
```

## license and attributions

Copyright 2022-2023 M. Farzalipour Tabriz, Max Planck Computing and Data Facility (MPCDF)
All rights reserved.

This software may be modified and distributed under the terms of the GNU Lesser General Public License 3.0. See the LICENSE file for details.
